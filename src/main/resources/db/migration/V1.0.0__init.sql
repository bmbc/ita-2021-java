create table author
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    bio         varchar(255),
    birth_date  date,
    name        varchar(255),
    primary key (id)
);
create table cart
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    primary key (id)
);
create table genre
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(255),
    name        varchar(255),
    primary key (id)
);
create table orders
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    address     varchar(255),
    name        varchar(255),
    status      varchar(255),
    primary key (id)
);
create table product
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(512),
    image       varchar(255),
    name        varchar(255),
    price       numeric(19, 2),
    stock       int8,
    id_author   int8,
    id_genre    int8,
    primary key (id)
);
create table r_cart_products
(
    id_card    int8 not null,
    id_product int8 not null
);
create table r_order_products
(
    id_order   int8 not null,
    id_product int8 not null
);
create sequence hibernate_sequence start 1000 increment 1;
alter table if exists product add constraint FKe3yi2ykjch3qavegh08srogmp foreign key (id_author) references author;
alter table if exists product add constraint FKrdfx1tx1fmt401ma20qli3xw0 foreign key (id_genre) references genre;
alter table if exists r_cart_products add constraint FK2b510g2v1woiq6qrqll0mlbd2 foreign key (id_product) references product;
alter table if exists r_cart_products add constraint FK4d7yechps0hkieupblgv76bgl foreign key (id_card) references cart;
alter table if exists r_order_products add constraint FKn51p8vub9s11pohnaci4g8mg3 foreign key (id_product) references product;
alter table if exists r_order_products add constraint FKo6p6h3myymbvahguk5l7kif2 foreign key (id_order) references orders;