insert into genre (id, created_at, modified_at, description, name)
values (1, TO_TIMESTAMP('2021-07-02 06:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-07-02 06:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'Bildungsroman is a literary genre that focuses on the psychological and moral growth of the protagonist from youth to adulthood.', 'Coming of Age');

insert into genre (id, created_at, modified_at, description, name)
values (2, TO_TIMESTAMP('2021-08-03 09:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-08-03 09:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'Horror is a genre of speculative fiction which is intended to frighten, scare, or disgust.', 'Horror');

insert into genre (id, created_at, modified_at, description, name)
values (3, TO_TIMESTAMP('2021-08-04 10:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-08-04 10:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'Thrillers are characterized and defined by the moods they elicit, giving viewers heightened feelings of suspense, excitement, surprise, anticipation and anxiety.', 'Thriller');

insert into author (id, created_at, modified_at, bio, birth_date, name)
values (1, TO_TIMESTAMP('2021-07-02 06:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-07-02 06:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'Abraham Stoker was an Irish author, best known today for his 1897 Gothic horror novel Dracula. During his lifetime, he was better known as the personal assistant of actor Sir Henry Irving and business manager of the Lyceum Theatre, which Irving owned.', TO_DATE('1847-11-08', 'YYYY-MM-DD'), 'Bram Stoker');

insert into author (id, created_at, modified_at, bio, birth_date, name)
values (2, TO_TIMESTAMP('2021-07-02 15:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-07-02 15:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'American writer known for her darkly entertaining tales of murder and deceit in the Midwest. Flynn, the younger of two children, was raised in Kansas City, where both of her parents taught.', TO_DATE('1971-02-24', 'YYYY-MM-DD'), 'Gillian Flynn');

insert into author (id, created_at, modified_at, bio, birth_date, name)
values (3, TO_TIMESTAMP('2021-06-12 22:14:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2021-06-12 22:14:00', 'YYYY-MM-DD HH24:MI:SS'), 'American writer whose novel The Catcher in the Rye (1951) won critical acclaim and devoted admirers, especially among the post-World War II generation of college students.', TO_DATE('1919-01-01', 'YYYY-MM-DD'), 'Jerome David Salinger');