package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.domain.Order;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.enumerations.Status;
import cz.cernicek.eshop.exception.CartNotFoundException;
import cz.cernicek.eshop.exception.OrderNotFoundException;
import cz.cernicek.eshop.mapper.OrderMapper;
import cz.cernicek.eshop.model.OrderDto;
import cz.cernicek.eshop.model.OrderRequestDto;
import cz.cernicek.eshop.repository.CartRepository;
import cz.cernicek.eshop.repository.OrderRepository;
import cz.cernicek.eshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final CartRepository cartRepository;

    @Override
    @Transactional
    public OrderDto createOrder(OrderRequestDto request) {

        final Long cartId = request.getIdCart();

        log.info("Creating order from cart id {}", cartId);
        final Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new CartNotFoundException(cartId));

        final List<Product> productList = cart.getProducts();

        final Order order = orderMapper.toEntity(request);

        order.setStatus(Status.NEW);
        order.setProducts(productList);

        cartRepository.delete(cart);

        final Order savedOrder = orderRepository.save(order);

        final OrderDto response = orderMapper.toDto(savedOrder);

        log.debug("Returning {}", response);

        return response;

    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderDto> findAll() {
        return orderRepository.findAll().stream()
                .map(orderMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OrderDto updateStatus(Long id, Status status) {
        log.info("Updating order with id {} to change status to {}", id, status);

        final Order order = orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));

        order.setStatus(status);

        final OrderDto updatedOrderDto = orderMapper.toDto(order);

        log.debug("Returning updated {}", updatedOrderDto);

        return updatedOrderDto;
    }
}
