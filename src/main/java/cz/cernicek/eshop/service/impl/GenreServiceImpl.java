package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.exception.GenreNotFoundException;
import cz.cernicek.eshop.mapper.GenreMapper;
import cz.cernicek.eshop.model.GenreDto;
import cz.cernicek.eshop.repository.GenreRepository;
import cz.cernicek.eshop.service.GenreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;
    private final GenreMapper genreMapper;

    @Override
    @Transactional(readOnly = true)
    public GenreDto getById(Long id) {
        final Genre byId = genreRepository.findById(id)
                .orElseThrow(() -> new GenreNotFoundException(id));

        final GenreDto genreDto = genreMapper.toDto(byId);

        log.debug("Returning {}", genreDto);

        return genreDto;

    }

    @Override
    @Transactional(readOnly = true)
    public List<GenreDto> getAll() {
        log.info("Searching for all genres");
        final List<GenreDto> returnedGenreDtos = genreRepository.findAll().stream()
                .map(genreMapper::toDto)
                .collect(Collectors.toList());

        log.debug("Returning {}", returnedGenreDtos);

        return returnedGenreDtos;
    }
}
