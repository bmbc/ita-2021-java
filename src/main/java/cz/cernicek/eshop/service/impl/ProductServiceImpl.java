package cz.cernicek.eshop.service.impl;


import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.exception.AuthorNotFoundException;
import cz.cernicek.eshop.exception.GenreNotFoundException;
import cz.cernicek.eshop.exception.ProductNotFoundException;
import cz.cernicek.eshop.mapper.ProductMapper;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import cz.cernicek.eshop.repository.AuthorRepository;
import cz.cernicek.eshop.repository.GenreRepository;
import cz.cernicek.eshop.repository.ProductRepository;
import cz.cernicek.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> getAll() {
        log.info("Searching for all products");
        final List<ProductDto> returnedProductDtos = productRepository.findAll().stream()
                .map(productMapper::mapToResponse)
                .collect(Collectors.toList());

        log.debug("Returning {}", returnedProductDtos);

        return returnedProductDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public ProductDto findById(Long id) {
        log.info("Searching for a product with id {}", id);
        final Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));

        final ProductDto productDto = productMapper.mapToResponse(product);

        log.debug("Returning {}", productDto);

        return productDto;
    }

    @Override
    @Transactional
    public ProductDto saveProduct(ProductRequestDto request) {
        log.info("Saving product");
        log.debug("Saving {}", request);

        final Author author = authorRepository.findById(request.getIdAuthor())
                .orElseThrow(() -> new AuthorNotFoundException(request.getIdAuthor()));

        final Genre genre = genreRepository.findById(request.getIdGenre())
                .orElseThrow(() -> new GenreNotFoundException(request.getIdGenre()));

        final Product product = productMapper.mapToEntity(request)
                .setAuthor(author)
                .setGenre(genre);

        final Product savedProduct =  productRepository.save(product);

        final ProductDto savedProductDto = productMapper.mapToResponse(savedProduct);
        
        log.debug("Returning saved {}", savedProductDto);

        return savedProductDto;
    }

    @Override
    @Transactional
    public ProductDto updateProduct(Long id, ProductRequestDto request) {
        log.info("Updating product with id {}", id);
        log.debug("Updating product with id {} and payload {}", id, request);

        if (!productRepository.existsById(id)) {
            throw new ProductNotFoundException(id);
        }

        final Author author = authorRepository.findById(request.getIdAuthor())
                .orElseThrow(() -> new AuthorNotFoundException(request.getIdAuthor()));

        final Genre genre = genreRepository.findById(request.getIdGenre())
                .orElseThrow(() -> new GenreNotFoundException(request.getIdGenre()));

        final Product product = (Product) productMapper.mapToEntity(request)
                .setId(id);

        product.setAuthor(author)
                .setGenre(genre);

        final Product savedProduct = productRepository.save(product);

        final ProductDto updatedProductDto = productMapper.mapToResponse(savedProduct);

        log.debug("Returning updated {}", updatedProductDto);

        return updatedProductDto;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        log.info("Deleting product id {}", id);
        if (!productRepository.existsById(id)) {
            throw new ProductNotFoundException(id);
        }

        productRepository.deleteById(id);

        log.debug("Product id {} deleted", id);
    }

}
