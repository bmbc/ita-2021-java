package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.exception.AuthorNotFoundException;
import cz.cernicek.eshop.mapper.AuthorMapper;
import cz.cernicek.eshop.model.AuthorDto;
import cz.cernicek.eshop.repository.AuthorRepository;
import cz.cernicek.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    @Transactional(readOnly = true)
    public AuthorDto getById(Long id) {
        final Author byId = authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));
        return authorMapper.toDto(byId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AuthorDto> getAll() {
        log.info("Searching for all authors");
        final List<AuthorDto> returnedAuthorDtos = authorRepository.findAll().stream()
                .map(authorMapper::toDto)
                .collect(Collectors.toList());

        log.debug("Returning {}", returnedAuthorDtos);

        return returnedAuthorDtos;
    }
}
