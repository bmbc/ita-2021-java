package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.exception.CartNotFoundException;
import cz.cernicek.eshop.exception.ProductNotFoundException;
import cz.cernicek.eshop.mapper.CartMapper;
import cz.cernicek.eshop.model.CartDto;
import cz.cernicek.eshop.repository.CartRepository;
import cz.cernicek.eshop.repository.ProductRepository;
import cz.cernicek.eshop.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {

    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final CartMapper cartMapper;

    @Override
    @Transactional
    public CartDto createCart(Long productId) {
        log.info("Creating cart with product id {} inside", productId);
        final Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));

        final Cart cart = new Cart()
                .setProducts(List.of(product));

        final Cart savedCart = cartRepository.save(cart);

        final CartDto savedCartDto = cartMapper.toDto(savedCart);

        log.debug("Returning saved {}", savedCartDto);

        return savedCartDto;
    }

    @Override
    @Transactional
    public CartDto addProduct(Long cartId, Long productId) {
        log.info("Adding product with id {} into cart id {}", productId, cartId);
        final Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new CartNotFoundException(cartId));

        final Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));

        List<Product> productList = new ArrayList<>(cart.getProducts());

        productList.add(product);

        cart.setProducts(productList);

        final CartDto savedCartDto = cartMapper.toDto(cart);

        log.debug("Returning saved {}", savedCartDto);

        return savedCartDto;
    }

    @Override
    @Transactional(readOnly = true)
    public CartDto getById(Long id) {
        log.info("Searching for a cart with id {}", id);
        final Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new CartNotFoundException(id));

        final CartDto cartDto = cartMapper.toDto(cart);

        log.debug("Returning {}", cartDto);

        return cartDto;
    }
}
