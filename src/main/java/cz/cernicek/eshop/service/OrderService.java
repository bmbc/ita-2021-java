package cz.cernicek.eshop.service;

import cz.cernicek.eshop.enumerations.Status;
import cz.cernicek.eshop.model.CartDto;
import cz.cernicek.eshop.model.OrderDto;
import cz.cernicek.eshop.model.OrderRequestDto;

import java.util.List;

public interface OrderService {
    OrderDto createOrder(OrderRequestDto request);

    List<OrderDto> findAll();

    OrderDto updateStatus(Long id, Status status);
}
