package cz.cernicek.eshop.service;

import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;

import java.util.List;

public interface ProductService {
    List<ProductDto> getAll();

    ProductDto findById(Long id);

    ProductDto saveProduct(ProductRequestDto request);

    ProductDto updateProduct(Long id, ProductRequestDto request);

    void deleteById(Long id);

}
