package cz.cernicek.eshop.service;

import cz.cernicek.eshop.model.CartDto;

public interface CartService {
    CartDto createCart(Long productId);

    CartDto addProduct(Long cartId, Long productId);

    CartDto getById(Long id);
}
