package cz.cernicek.eshop.service;

import cz.cernicek.eshop.model.GenreDto;

import java.util.List;

public interface GenreService {
    GenreDto getById(Long id);

    List<GenreDto> getAll();
}
