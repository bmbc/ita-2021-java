package cz.cernicek.eshop.service;

import cz.cernicek.eshop.model.AuthorDto;

import java.util.List;

public interface AuthorService {
    AuthorDto getById(Long id);

    List<AuthorDto> getAll();
}
