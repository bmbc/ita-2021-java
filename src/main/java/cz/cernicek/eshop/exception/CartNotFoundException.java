package cz.cernicek.eshop.exception;

import org.springframework.http.HttpStatus;

public class CartNotFoundException extends ItaException {
    public CartNotFoundException(long id) {
        super("Cart with id " + id + " was not found", HttpStatus.NOT_FOUND);
    }
}