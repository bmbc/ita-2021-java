package cz.cernicek.eshop.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends ItaException {

    public ProductNotFoundException(long id) {
        super("Product " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
