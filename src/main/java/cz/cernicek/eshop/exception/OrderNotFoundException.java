package cz.cernicek.eshop.exception;

import org.springframework.http.HttpStatus;

public class OrderNotFoundException extends ItaException {

    public OrderNotFoundException(long id) {
        super("Order " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
