package cz.cernicek.eshop.exception;

import org.springframework.http.HttpStatus;

public class AuthorNotFoundException extends ItaException {
    public AuthorNotFoundException(long id) {
        super("Author with id " + id + " was not found", HttpStatus.NOT_FOUND);
    }
}