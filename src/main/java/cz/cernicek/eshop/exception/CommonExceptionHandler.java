package cz.cernicek.eshop.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ItaException.class)
    public ResponseEntity<Object> handleItaException(ItaException ex, ServletWebRequest webRequest) {

        log.error("An error occurred during processing request " + webRequest.getRequest().getMethod()
                + " at " + webRequest.getRequest().getRequestURL(), ex);

        return handleExceptionInternal(ex, new ExceptionResponseDto()
                        .setMessage(ex.getMessage())
                        .setStatus(ex.getStatus().value()),
                new HttpHeaders(), ex.getStatus(), webRequest);

    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(Exception ex, ServletWebRequest webRequest) {

      log.error("An error occurred during processing request " + webRequest.getRequest().getMethod()
              + " at " + webRequest.getRequest().getRequestURL(), ex);

      return handleExceptionInternal(ex, new ExceptionResponseDto()
              .setMessage(ex.getMessage())
              .setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()),
              new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, webRequest);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {


        log.error("Wrong input parameters: ", exception);

        final String errors = exception.getBindingResult().getAllErrors().stream()
                .map(error -> ((FieldError) error).getField() + " " + error.getDefaultMessage())
                .collect(Collectors.joining(", "));

        return new ResponseEntity(
                new ExceptionResponseDto()
                    .setStatus(HttpStatus.BAD_REQUEST.value())
                    .setMessage(errors),
                headers,
                status);
    }


}
