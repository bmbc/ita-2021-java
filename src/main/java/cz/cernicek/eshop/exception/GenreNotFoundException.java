package cz.cernicek.eshop.exception;

import org.springframework.http.HttpStatus;

public class GenreNotFoundException extends ItaException {
    public GenreNotFoundException(long id) {
        super("Genre with id " + id + " was not found", HttpStatus.NOT_FOUND);
    }
}
