package cz.cernicek.eshop.model;

import lombok.Data;

@Data
public class GenreDto {
    private Long id;
    private String name;
    private String description;
}
