package cz.cernicek.eshop.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;
    private AuthorDto author;
    private GenreDto genre;
}
