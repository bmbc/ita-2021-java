package cz.cernicek.eshop.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class OrderRequestDto {
    @NotNull
    @Positive
    private Long idCart;
    @NotBlank
    private String name;
    @NotBlank
    private String address;
}
