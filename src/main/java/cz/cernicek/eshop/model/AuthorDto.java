package cz.cernicek.eshop.model;

import lombok.Data;


@Data
public class AuthorDto {
    private Long id;
    private String name;
    private String bio;
}
