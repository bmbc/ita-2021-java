package cz.cernicek.eshop.model;

import cz.cernicek.eshop.enumerations.Status;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
public class OrderDto {

    private Long id;
    @Enumerated(EnumType.STRING)
    private Status orderStatus;
    private String name;
    private String address;
    private List<ProductDto> products;
}
