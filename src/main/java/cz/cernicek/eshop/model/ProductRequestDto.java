package cz.cernicek.eshop.model;

import cz.cernicek.eshop.validation.constraints.StartsWithUpperCase;
import lombok.Data;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
public class ProductRequestDto {

    @NotBlank
    @Size(max = 256)
    @StartsWithUpperCase
    private String name;

    @NotBlank
    @Size(max = 256)
    private String description;

    @NotNull
    @Positive
    private BigDecimal price;

    @NotNull
    @PositiveOrZero
    private Long stock;

    @NotBlank
    private String image;

    @NotNull
    @Positive
    private Long idAuthor;

    @NotNull
    @Positive
    private Long idGenre;
}
