package cz.cernicek.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;
import java.time.LocalDate;


@Entity
@Data
public class Author extends AbstractEntity{
    private String name;
    private String bio;
}
