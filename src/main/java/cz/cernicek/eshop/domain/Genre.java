package cz.cernicek.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Genre extends AbstractEntity {
    private String name;
    private String description;
}
