package cz.cernicek.eshop.domain;

import cz.cernicek.eshop.enumerations.Status;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Orders")
@Data
public class Order extends AbstractEntity{

    private String name;
    private String address;
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany(cascade= CascadeType.PERSIST)
    @JoinTable(
            name ="r_order_products",
            joinColumns = { @JoinColumn(name = "id_order") },
            inverseJoinColumns = { @JoinColumn(name ="id_product") }
    )
    private List<Product> products;

}
