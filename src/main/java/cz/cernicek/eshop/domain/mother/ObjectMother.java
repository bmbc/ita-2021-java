package cz.cernicek.eshop.domain.mother;

import cz.cernicek.eshop.domain.*;
import cz.cernicek.eshop.enumerations.Status;
import cz.cernicek.eshop.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class ObjectMother {
    //author
    public static Author createAuthorWithId(Long wantedId) {
        return (Author) new Author()
                .setName("Charles Bukowski")
                .setBio("likes drinking and women")
                .setId(wantedId)
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ));
    }

    public static AuthorDto createAuthorDtoWithId(Long wantedId) {
        return new AuthorDto()
                .setName("Charles Bukowski")
                .setBio("likes drinking and women")
                .setId(wantedId);
    }

    //genre

    public static Genre createGenreWithId(Long wantedId) {
        return (Genre) new Genre()
                .setName("Horror")
                .setDescription("Horror is a genre of speculative fiction which " +
                        "is intended to frighten, scare, or disgust.")
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setId(wantedId);
    }

    public static GenreDto createGenreDtoWithId(Long wantedId) {
        return new GenreDto()
                .setName("Horror")
                .setDescription("Horror is a genre of speculative fiction which " +
                        "is intended to frighten, scare, or disgust.")
                .setId(wantedId);
    }

    //product
    private static final BigDecimal price = BigDecimal.valueOf(120);
    private static final Long stock = 15L;
    private static final String name = "The Picture Of Dorian Gray";
    private static final String description = "In this celebrated work, " +
            "his only novel, Wilde forged a devastating portrait of " +
            "the effects of evil and debauchery on a young aesthete in " +
            "late-19th-century England.";
    private static final String image = "https://images-na.ssl-images-amazon.com/images/I/4129Vd3SZ9L._SX352_BO1,204,203,200_.jpg";


    public static ProductDto createProductDtoWithId(Long wantedId) {
        return new ProductDto()
                .setId(wantedId)
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setAuthor(createAuthorDtoWithId(1L))
                .setGenre(createGenreDtoWithId(1L));
    }

    public static ProductRequestDto createProductRequest() {
        return new ProductRequestDto()
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setIdAuthor(1L)
                .setIdGenre(1L);
    }

    public static Product createProductWithId(Long wantedId) {
        return (Product) new Product()
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setAuthor(createAuthorWithId(1L))
                .setGenre(createGenreWithId(1L))
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setId(wantedId);
    }

    public static Product createProduct() {
        return new Product()
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setAuthor(createAuthorWithId(1L))
                .setGenre(createGenreWithId(1L));
    }

    public static Product createProductWithNullTimestamps(Long wantedId) {
        return (Product) new Product()
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setAuthor(createAuthorWithId(1L))
                .setGenre(createGenreWithId(1L))
                .setId(wantedId);
    }

    public static Product createMappedProduct() {
        return new Product()
                .setName(name)
                .setDescription(description)
                .setPrice(price)
                .setStock(stock)
                .setImage(image)
                .setAuthor(createAuthorWithId(1L))
                .setGenre(createGenreWithId(1L));
    }

    //cart
    public static Cart createCartWithThreeProducts() {
        return (Cart) new Cart()
                .setProducts(
                        List.of(createProductWithId(1L),
                                createProductWithId(2L),
                                createProductWithId(3L)))
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setId(1L);
    }

    public static CartDto createCartDtoWithThreeProducts() {
        return new CartDto()
                .setProducts(
                        List.of(createProductDtoWithId(1L),
                                createProductDtoWithId(2L),
                                createProductDtoWithId(3L)))
                .setId(1L);
    }

    public static CartDto createCartDtoWithFourProducts() {
        return new CartDto()
                .setProducts(
                        List.of(createProductDtoWithId(1L),
                                createProductDtoWithId(2L),
                                createProductDtoWithId(3L),
                                createProductDtoWithId(4L)
                                ))
                .setId(1L);
    }

    public static Cart createCartWithProducts(List<Product> productsToAdd) {
        return (Cart) new Cart()
                .setProducts(productsToAdd)
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setId(1L);
    }

    public static CartDto createCartDtoWithProducts(List<ProductDto> productsToAdd) {
        return new CartDto()
                .setProducts(productsToAdd)
                .setId(1L);
    }

    // Order
    private static final String orderName = "Ted Kaczynski";
    private static final String orderAddress = "United States Penitentiary, Fremont County";
    private static final Long orderId = 1L;
    private static final Status orderStatus = Status.NEW;

    public static Order createOrderForTesting() {

        return (Order) new Order()
                .setName(orderName)
                .setAddress(orderAddress)
                .setStatus(orderStatus)
                .setProducts(
                        List.of(createProductWithId(1L),
                                createProductWithId(2L),
                                createProductWithId(3L)))
                .setId(orderId)
                .setCreatedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ))
                .setModifiedAt(LocalDateTime.of(2021, 8, 15, 10, 10, 10 ));

    }
    public static Order createMappedOrderForTesting() {
        return new Order()
                .setName(orderName)
                .setAddress(orderAddress)
                .setStatus(orderStatus)
                .setProducts(
                        List.of(createProductWithId(1L),
                                createProductWithId(2L),
                                createProductWithId(3L)));

    }


    public static OrderDto createOrderDtoForTesting() {
        return new OrderDto()
                .setName(orderName)
                .setAddress(orderAddress)
                .setOrderStatus(orderStatus)
                .setProducts(
                        List.of(createProductDtoWithId(1L),
                                createProductDtoWithId(2L),
                                createProductDtoWithId(3L)))
                .setId(orderId);
    }



    public static OrderRequestDto createOrderRequestDtoForTesting(Long id) {
        return new OrderRequestDto()
                .setName(orderName)
                .setAddress(orderAddress)
                .setIdCart(id);
    }

}
