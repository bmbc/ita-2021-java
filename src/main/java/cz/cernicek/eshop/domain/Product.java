package cz.cernicek.eshop.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class Product extends AbstractEntity{
    private String name;
    @Column(length = 512)
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;

    @ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_author")
    private Author author;

    @ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_genre")
    private Genre genre;

}
