package cz.cernicek.eshop.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Cart extends AbstractEntity {
    @ManyToMany(cascade= CascadeType.PERSIST)
    @JoinTable(
            name ="r_cart_products",
            joinColumns = { @JoinColumn(name = "id_card") },
            inverseJoinColumns = { @JoinColumn(name ="id_product") }
    )
    private List<Product> products;
}
