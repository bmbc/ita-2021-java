package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
