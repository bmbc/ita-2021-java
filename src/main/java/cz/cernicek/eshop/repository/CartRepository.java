package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
}
