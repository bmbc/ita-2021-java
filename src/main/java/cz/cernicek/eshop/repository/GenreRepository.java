package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
