package cz.cernicek.eshop.validation.validators;

import cz.cernicek.eshop.validation.constraints.StartsWithUpperCase;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StartsWithUpperCaseValidator implements ConstraintValidator<StartsWithUpperCase, String> {

    @Override
    public void initialize(StartsWithUpperCase constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value,
                           ConstraintValidatorContext cxt) {

        return value != null && !value.isEmpty() && Character.isUpperCase(value.charAt(0));
    }

}