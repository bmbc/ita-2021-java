package cz.cernicek.eshop.validation.constraints;

import cz.cernicek.eshop.validation.validators.StartsWithUpperCaseValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StartsWithUpperCaseValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface StartsWithUpperCase {
    String message() default "must start with upper case letter";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
