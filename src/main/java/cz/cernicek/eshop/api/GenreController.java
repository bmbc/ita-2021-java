package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.GenreDto;
import cz.cernicek.eshop.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/genres")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService service;

    @GetMapping
    public List<GenreDto> getAll() {
        return service.getAll();
    }

    @GetMapping("{id}")
    public GenreDto getById(@PathVariable(name = "id") Long id) {
        return service.getById(id);
    }

}
