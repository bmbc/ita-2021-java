package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.CartDto;
import cz.cernicek.eshop.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService service;

    @GetMapping("{id}")
    public CartDto getById(@PathVariable(name = "id") Long id) {

        return service.getById(id);
    }

    @PostMapping("product/{id}")
    public CartDto createCart(@PathVariable(name = "id") Long id) {
        return service.createCart(id);
    }

    @PutMapping("{cartId}/product/{productId}")
    public CartDto addProduct(@PathVariable("cartId") Long cartId,
                              @PathVariable("productId") Long productId) {
        return service.addProduct(cartId, productId);
    }


}
