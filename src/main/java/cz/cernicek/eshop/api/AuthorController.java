package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.AuthorDto;
import cz.cernicek.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService service;

    @GetMapping
    public List<AuthorDto> getAll() {
        return service.getAll();
    }

    @GetMapping("{id}")
    public AuthorDto getById(@PathVariable(name = "id") Long id) {
        return service.getById(id);
    }
}
