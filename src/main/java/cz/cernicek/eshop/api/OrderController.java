package cz.cernicek.eshop.api;

import cz.cernicek.eshop.enumerations.Status;
import cz.cernicek.eshop.model.OrderDto;
import cz.cernicek.eshop.model.OrderRequestDto;
import cz.cernicek.eshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public OrderDto createOrder(@Valid @RequestBody OrderRequestDto request) {
        return orderService.createOrder(request);
    }

    @GetMapping
    public List<OrderDto> findAll() {
        return orderService.findAll();
    }

    @PutMapping("{id}/status/{status}")
    public OrderDto updateStatus(@PathVariable("id") Long id,
                              @PathVariable("status") Status status) {
        return orderService.updateStatus(id, status);
    }
}
