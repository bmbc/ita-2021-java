package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import cz.cernicek.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;


    @GetMapping
    public List<ProductDto> getAll() {
        return productService.getAll();
    }

    @GetMapping("{id}")
    public ProductDto findById(@PathVariable("id") Long id) {
        return productService.findById(id);
    }

    @PutMapping("{id}")
    public ProductDto updateProduct(@PathVariable("id") Long id,
                                    @Valid @RequestBody ProductRequestDto request) {
        return productService.updateProduct(id, request);
    }

    @PostMapping
    public ProductDto saveProduct(@Valid @RequestBody ProductRequestDto request) {
        return productService.saveProduct(request);
    }


    @DeleteMapping("{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        productService.deleteById(id);
    }


}
