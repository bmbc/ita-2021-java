package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.model.CartDto;
import org.mapstruct.Mapper;

@Mapper
public interface CartMapper {
    CartDto toDto(Cart cart);
}
