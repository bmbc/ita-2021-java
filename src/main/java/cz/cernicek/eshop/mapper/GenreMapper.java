package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.model.GenreDto;
import org.mapstruct.Mapper;

@Mapper
public interface GenreMapper {
    GenreDto toDto(Genre genre);
}
