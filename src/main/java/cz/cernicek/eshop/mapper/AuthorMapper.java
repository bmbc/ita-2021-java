package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.model.AuthorDto;
import org.mapstruct.Mapper;

@Mapper
public interface AuthorMapper {
    AuthorDto toDto(Author author);
}
