package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import org.mapstruct.Mapper;

@Mapper//(uses = AuthorMapper.class)
public interface ProductMapper {

    Product mapToEntity(ProductRequestDto request);
    ProductDto mapToResponse(Product product);

}
