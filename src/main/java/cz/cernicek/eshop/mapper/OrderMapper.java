package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Order;
import cz.cernicek.eshop.model.OrderDto;
import cz.cernicek.eshop.model.OrderRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface OrderMapper {
    @Mapping(target = "orderStatus", source = "status")
    OrderDto toDto(Order order);
    Order toEntity(OrderRequestDto request);
}
