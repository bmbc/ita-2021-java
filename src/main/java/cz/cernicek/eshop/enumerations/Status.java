package cz.cernicek.eshop.enumerations;

public enum Status {
    NEW, COMPLETED, CANCELLED
}
