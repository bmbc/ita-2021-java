package cz.cernicek.eshop.mapper;

import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ProductMapperTest {


    private final ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

    @Test
    void mapToEntity() {
        // arrange
        final ProductRequestDto request = new ProductRequestDto()
                .setName("The Picture Of Dorian Gray")
                .setDescription("In this celebrated work, " +
                        "his only novel, Wilde forged a devastating portrait of " +
                        "the effects of evil and debauchery on a young aesthete in " +
                        "late-19th-century England.")
                .setPrice(BigDecimal.valueOf(120L))
                .setStock(15L)
                .setImage("https://images-na.ssl-images-amazon.com/images/I/4129Vd3SZ9L._SX352_BO1,204,203,200_.jpg")
                .setIdAuthor(1L)
                .setIdGenre(1L);

        // act
        final Product product = productMapper.mapToEntity(request);

        // assert
        assertThat(product).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(product.getName()).isEqualTo(request.getName());
            softAssertions.assertThat(product.getDescription()).isEqualTo(request.getDescription());
            softAssertions.assertThat(product.getPrice()).isEqualTo(request.getPrice());
            softAssertions.assertThat(product.getStock()).isEqualTo(request.getStock());
            softAssertions.assertThat(product.getImage()).isEqualTo(request.getImage());
            softAssertions.assertThat(product.getAuthor()).isNull();
            softAssertions.assertThat(product.getGenre()).isNull();

        });
    }

    @Test
    @DisplayName("Test should pass when mapToEntity method called with null argument" +
            " returns null")
    void mapToEntity_nullInputReturnsNull() {
        final Product product = productMapper.mapToEntity(null);
        assertThat(product).isNull();
    }

    @Test
    void mapToResponse() {
        // arrange
        final Product product = new Product()
                .setName("Post Office")
                .setDescription("This classic 1971 novel—the one that catapulted its author" +
                        " to national fame—is the perfect introduction to the grimly hysterical" +
                        " world of legendary writer, poet, and Dirty Old Man Charles Bukowski and " +
                        "his fictional alter ego, Chinaski.")
                .setPrice(BigDecimal.valueOf(26))
                .setStock(8L)
                .setImage("https://images-na.ssl-images-amazon.com/images/I/516vm9D3yFS._SX330_BO1,204,203,200_.jpg");

        // act
        final ProductDto response = productMapper.mapToResponse(product);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getName()).isEqualTo(product.getName());
            softAssertions.assertThat(response.getDescription()).isEqualTo(product.getDescription());
            softAssertions.assertThat(response.getPrice()).isEqualTo(product.getPrice());
            softAssertions.assertThat(response.getStock()).isEqualTo(product.getStock());
            softAssertions.assertThat(response.getImage()).isEqualTo(product.getImage());
        });
    }

    @Test
    @DisplayName("Test should pass when mapToResponse method called with null argument" +
            " returns null")
    void mapToResponse_nullInputReturnsNull() {
        final ProductDto response = productMapper.mapToResponse(null);
        assertThat(response).isNull();
    }
}