package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Genre;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

@DataJpaTest
class GenreRepositoryIT implements WithAssertions {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GenreRepository repository;

    @Test
    void save() {
       final Genre genre = new Genre()
               .setName("Crime fiction")
               .setDescription("Narratives that centre on criminal acts " +
                       "and especially on the investigation, " +
                       "either by an amateur or a professional detective, " +
                       "of a serious crime, generally a murder");

       final Genre persistedGenre = entityManager.persistAndFlush(genre);

       entityManager.detach(persistedGenre);

       final Optional<Genre> optionalGenre =
               repository.findById(persistedGenre.getId());

       assertThat(optionalGenre).isNotEmpty();

       final Genre retrievedGenre = optionalGenre.get();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(retrievedGenre.getName())
                    .isEqualTo(genre.getName());
            softAssertions.assertThat(retrievedGenre.getDescription())
                    .isEqualTo(genre.getDescription());
            softAssertions.assertThat(retrievedGenre.getId()).isNotNull();
            softAssertions.assertThat(retrievedGenre.getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrievedGenre.getModifiedAt()).isNotNull();
        });


    }

}