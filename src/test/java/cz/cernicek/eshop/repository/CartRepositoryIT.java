package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.domain.Product;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

@DataJpaTest
class CartRepositoryIT implements WithAssertions {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CartRepository repository;

    @Test
    void save() {

        final Product product1 = new Product()
                .setName("Test product 1");

        final Product product2 = new Product()
                .setName("Test product 1");

        final List<Product> listOfProducts = List.of(product1, product2);

        final Cart cart = new Cart()
                .setProducts(listOfProducts);

        final Cart persistedCart = entityManager.persistAndFlush(cart);

        entityManager.detach(persistedCart);

        final Optional<Cart> optionalCart = repository
                .findById(persistedCart.getId());

        assertThat(optionalCart).isNotEmpty();

        final Cart retrievedCart = optionalCart.get();

        SoftAssertions.assertSoftly(softAssertions -> {

            softAssertions.assertThat(retrievedCart.getProducts().get(0)
            .getName())
                    .isEqualTo(cart.getProducts().get(0).getName());

            softAssertions.assertThat(retrievedCart.getProducts().get(0)
                    .getId())
                    .isNotNull();

            softAssertions.assertThat(retrievedCart.getProducts().get(0)
                    .getCreatedAt())
                    .isNotNull();

            softAssertions.assertThat(retrievedCart.getProducts().get(0)
                    .getModifiedAt())
                    .isNotNull();


            softAssertions.assertThat(retrievedCart.getProducts().get(1)
                    .getName())
                    .isEqualTo(cart.getProducts().get(1).getName());

            softAssertions.assertThat(retrievedCart.getProducts().get(1)
                    .getId())
                    .isNotNull();

            softAssertions.assertThat(retrievedCart.getProducts().get(1)
                    .getCreatedAt())
                    .isNotNull();

            softAssertions.assertThat(retrievedCart.getProducts().get(1)
                    .getModifiedAt())
                    .isNotNull();

            softAssertions.assertThat(retrievedCart.getId()).isNotNull();
            softAssertions.assertThat(retrievedCart.getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrievedCart.getModifiedAt()).isNotNull();
        });

    }

}