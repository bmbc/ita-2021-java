package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Order;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.enumerations.Status;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

@DataJpaTest
class OrderRepositoryIT implements WithAssertions {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OrderRepository repository;

    @Test
    void save() {

        final Product product1 = new Product()
                .setName("Test product 1");

        final Product product2 = new Product()
                .setName("Test product 1");

        final List<Product> listOfProducts = List.of(product1, product2);

        final Order order = new Order()
                .setName("Some Name")
                .setAddress("Just some address")
                .setStatus(Status.NEW)
                .setProducts(listOfProducts);

        final Order persistedOrder = entityManager.persistAndFlush(order);

        entityManager.detach(persistedOrder);

        final Optional<Order> optionalOrder =
                repository.findById(persistedOrder.getId());

        assertThat(optionalOrder).isNotEmpty();

        final Order retrieverOrder = optionalOrder.get();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(retrieverOrder.getName())
                    .isEqualTo(order.getName());
            softAssertions.assertThat(retrieverOrder.getName())
                    .isEqualTo(order.getName());
            softAssertions.assertThat(retrieverOrder.getStatus())
                    .isEqualTo(order.getStatus());

            softAssertions.assertThat(retrieverOrder.getId()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getModifiedAt()).isNotNull();

            softAssertions.assertThat(retrieverOrder.getProducts().get(0).getId()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getProducts().get(0).getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getProducts().get(0).getModifiedAt()).isNotNull();

            softAssertions.assertThat(retrieverOrder.getProducts().get(1).getId()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getProducts().get(1).getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrieverOrder.getProducts().get(1).getModifiedAt()).isNotNull();
        });

    }


}