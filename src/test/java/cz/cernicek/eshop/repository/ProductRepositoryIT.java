package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.domain.Product;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.Optional;

@DataJpaTest
class ProductRepositoryIT implements WithAssertions {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository repository;

    @Test
    void save() {

        final Genre genre = new Genre()
                .setName("Crime fiction")
                .setDescription("Narratives that centre on criminal acts " +
                        "and especially on the investigation, " +
                        "either by an amateur or a professional detective, " +
                        "of a serious crime, generally a murder");

        final Author author = new Author()
                .setName("Charles Bukowski")
                .setBio("American author noted for his use of violent images " +
                        "and graphic language in poetry and fiction that depict " +
                        "survival in a corrupt, blighted society.");

        final Product product = new Product()
                .setName("Some product")
                .setDescription("Some description")
                .setImage("https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1424999238l/51504.jpg")
                .setStock(10L)
                .setPrice(BigDecimal.valueOf(150))
                .setAuthor(author)
                .setGenre(genre);

        final Product persistedProduct = entityManager.persistAndFlush(product);

        entityManager.detach(persistedProduct);

        final Optional<Product> optionalProduct =
                repository.findById(persistedProduct.getId());

        assertThat(optionalProduct).isNotEmpty();

        final Product retrievedProduct = optionalProduct.get();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(retrievedProduct.getName())
                    .isEqualTo(product.getName());
            softAssertions.assertThat(retrievedProduct.getDescription())
                    .isEqualTo(product.getDescription());
            softAssertions.assertThat(retrievedProduct.getImage())
                    .isEqualTo(product.getImage());
            softAssertions.assertThat(retrievedProduct.getStock())
                    .isEqualTo(product.getStock());
            softAssertions.assertThat(retrievedProduct.getPrice())
                    .isEqualByComparingTo(product.getPrice());

            softAssertions.assertThat(retrievedProduct.getId()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getModifiedAt()).isNotNull();


            softAssertions.assertThat(retrievedProduct.getAuthor().getName())
                    .isEqualTo(product.getAuthor().getName());
            softAssertions.assertThat(retrievedProduct.getAuthor().getBio())
                    .isEqualTo(product.getAuthor().getBio());
            softAssertions.assertThat(retrievedProduct.getAuthor().getId()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getAuthor().getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getAuthor().getModifiedAt()).isNotNull();

            softAssertions.assertThat(retrievedProduct.getGenre().getName())
                    .isEqualTo(product.getGenre().getName());
            softAssertions.assertThat(retrievedProduct.getGenre().getDescription())
                    .isEqualTo(product.getGenre().getDescription());
            softAssertions.assertThat(retrievedProduct.getGenre().getId()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getGenre().getCreatedAt()).isNotNull();
            softAssertions.assertThat(retrievedProduct.getGenre().getModifiedAt()).isNotNull();

        });


    }

}