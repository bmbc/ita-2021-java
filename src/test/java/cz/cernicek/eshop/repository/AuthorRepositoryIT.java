package cz.cernicek.eshop.repository;

import cz.cernicek.eshop.domain.Author;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

@DataJpaTest
class AuthorRepositoryIT implements WithAssertions {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuthorRepository repository;

    @Test
    void save() {

        final Author author = new Author()
                .setName("Charles Bukowski")
                .setBio("American author noted for his use of violent images " +
                        "and graphic language in poetry and fiction that depict " +
                        "survival in a corrupt, blighted society.");

        final Author persistedAuthor = entityManager.persistAndFlush(author);

        entityManager.detach(persistedAuthor);

        final Optional<Author> optionalAuthor = repository
                .findById(persistedAuthor.getId());

        assertThat(optionalAuthor).isNotEmpty();

        final Author retrievedAuthor = optionalAuthor.get();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(retrievedAuthor.getName())
                    .isEqualTo(author.getName());
            softAssertions.assertThat(retrievedAuthor.getBio())
                    .isEqualTo(author.getBio());
            softAssertions.assertThat(retrievedAuthor.getId())
                    .isNotNull();
        });


    }

}