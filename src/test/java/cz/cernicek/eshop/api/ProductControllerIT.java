package cz.cernicek.eshop.api;

import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import cz.cernicek.eshop.repository.ProductRepository;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static cz.cernicek.eshop.domain.mother.ObjectMother.createProductRequest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProductRepository productRepository;


    @Test
    void getAll() {

        final Product savedProduct1 = productRepository.save(new Product());
        final Product savedProduct2 = productRepository.save(new Product());
        final Product savedProduct3 = productRepository.save(new Product());

        final ResponseEntity<ProductDto[]> response = testRestTemplate.getForEntity("/api/v1/products", ProductDto[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final ProductDto[] body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body).hasSize(3);

        productRepository.deleteById(savedProduct1.getId());
        productRepository.deleteById(savedProduct2.getId());
        productRepository.deleteById(savedProduct3.getId());

    }

    @Test
    void findById() {
        final Product savedProduct1 = productRepository.save(new Product());

        final ResponseEntity<ProductDto> response = testRestTemplate
                .getForEntity("/api/v1/products/" + savedProduct1.getId(), ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final ProductDto body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body.getId()).isEqualTo(savedProduct1.getId());

        productRepository.deleteById(savedProduct1.getId());

    }

    @Test
    void findById_notFound() {

        final ResponseEntity<ProductDto> response = testRestTemplate
                .getForEntity("/api/v1/products/50", ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }


    @Test
    void updateProduct() {

        final Product savedProduct1 = productRepository.save(new Product());

        ProductRequestDto productRequestDto = createProductRequest();

        HttpEntity<ProductRequestDto> request = new HttpEntity<>(productRequestDto);


        final ResponseEntity<ProductDto> response = testRestTemplate
                .exchange("/api/v1/products/" + savedProduct1.getId(), HttpMethod.PUT, request, ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final ProductDto body = response.getBody();
        assertThat(body).isNotNull();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(body.getId()).isEqualTo(savedProduct1.getId());
            softAssertions.assertThat(body.getName()).isEqualTo(productRequestDto.getName());
            softAssertions.assertThat(body.getDescription()).isEqualTo(productRequestDto.getDescription());
            softAssertions.assertThat(body.getImage()).isEqualTo(productRequestDto.getImage());
            softAssertions.assertThat(body.getPrice()).isEqualTo(productRequestDto.getPrice());
            softAssertions.assertThat(body.getStock()).isEqualTo(productRequestDto.getStock());
            softAssertions.assertThat(body.getAuthor().getId()).isEqualTo(productRequestDto.getIdAuthor());
            softAssertions.assertThat(body.getGenre().getId()).isEqualTo(productRequestDto.getIdGenre());
        });

        productRepository.deleteById(savedProduct1.getId());


    }

    @Test
    void updateProduct_notFound() {

        ProductRequestDto productRequestDto = createProductRequest();

        HttpEntity<ProductRequestDto> request = new HttpEntity<>(productRequestDto);

        final ResponseEntity<ProductDto> response = testRestTemplate
                .exchange("/api/v1/products/6548", HttpMethod.PUT, request, ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);


    }

    @Test
    void saveProduct() {
        ProductRequestDto productRequestDto = createProductRequest();

        HttpEntity<ProductRequestDto> request = new HttpEntity<>(productRequestDto);

        final ResponseEntity<ProductDto> response = testRestTemplate
                .postForEntity("/api/v1/products", request, ProductDto.class );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final ProductDto body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body.getId()).isNotNull();

        productRepository.deleteById(body.getId());

    }

    @Test
    void deleteById() {

        final Product savedProduct1 = productRepository.save(new Product());
        assertThat(productRepository.findById(savedProduct1.getId())).isNotEmpty();

        HttpEntity<ProductDto> request = new HttpEntity<>(new ProductDto());


        final ResponseEntity<ProductDto> response = testRestTemplate
               .exchange("/api/v1/products/" + savedProduct1.getId(), HttpMethod.DELETE, request, ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(productRepository.findById(savedProduct1.getId())).isEmpty();


    }

    @Test
    void deleteById_notFound() {

        HttpEntity<ProductDto> request = new HttpEntity<>(new ProductDto());


        final ResponseEntity<ProductDto> response = testRestTemplate
                .exchange("/api/v1/products/658", HttpMethod.DELETE, request, ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }



}