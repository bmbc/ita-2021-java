package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.GenreDto;
import cz.cernicek.eshop.service.GenreService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.mapping.JpaMetamodelMappingContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static cz.cernicek.eshop.domain.mother.ObjectMother.createGenreDtoWithId;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({GenreController.class, BasicAuthenticationEntryPoint.class})
@MockBean(JpaMetamodelMappingContext.class)
class GenreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GenreService genreService;

    @Test
    @WithMockUser(username = "admin", password = "123", roles = "ADMIN")
    void getAll() throws Exception {
        final GenreDto genreDto1 = createGenreDtoWithId(1L);
        final GenreDto genreDto2 = createGenreDtoWithId(2L);
        final GenreDto genreDto3 = createGenreDtoWithId(3L);

        when(genreService.getAll())
                .thenReturn(List.of(genreDto1, genreDto2, genreDto3));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/genres"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]").value(genreDto1))
                .andExpect(jsonPath("$[1]").value(genreDto2))
                .andExpect(jsonPath("$[2]").value(genreDto3));

    }

    @Test
    @WithMockUser(username = "admin", password = "123", roles = "ADMIN")
    void getById() throws Exception {

        final GenreDto genreDto1 = createGenreDtoWithId(1L);

        when(genreService.getById(genreDto1.getId()))
                .thenReturn(genreDto1);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/genres/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(genreDto1));

    }
}