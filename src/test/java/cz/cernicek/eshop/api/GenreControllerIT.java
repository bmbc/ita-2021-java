package cz.cernicek.eshop.api;

import cz.cernicek.eshop.model.GenreDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GenreControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void getAll() {

        final ResponseEntity<GenreDto[]> response = testRestTemplate.getForEntity("/api/v1/genres", GenreDto[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final GenreDto[] body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body).hasSize(3);
    }

    @Test
    void getById() {
    }
}