package cz.cernicek.eshop.validation.validators;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class StartsWithUpperCaseValidatorTest {

    private StartsWithUpperCaseValidator validator = new StartsWithUpperCaseValidator();

    @ParameterizedTest
    @MethodSource
    void isValid(String bookName) {
        boolean result = validator.isValid(bookName, null);

        assertThat(result).isTrue();

    }

    static Object[] isValid() {
        return new Object[] {
                "Post Office",
                "People We Meet on Vacation",
                "The Picture of Dorian Gray",
                "The Unbearable Lightness of Being",
                "A",
                "B",

        };
    }

    @Test
    void isValid_oneLowerCaseLetter() {
        boolean result = validator.isValid("c", null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_whiteSpaceAndUpperCaseLetter() {
        boolean result = validator.isValid(" B", null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_whiteSpaceAndLowerCaseLetter() {
        boolean result = validator.isValid(" c", null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_number() {
        boolean result = validator.isValid("1", null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_null() {
        boolean result = validator.isValid(null, null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_emptyString() {
        boolean result = validator.isValid("", null);

        assertThat(result).isFalse();
    }

    @Test
    void isValid_whiteSpace() {
        boolean result = validator.isValid(" ", null);

        assertThat(result).isFalse();
    }

}