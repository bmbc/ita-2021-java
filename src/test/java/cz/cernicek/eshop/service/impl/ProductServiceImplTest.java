package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.exception.AuthorNotFoundException;
import cz.cernicek.eshop.exception.GenreNotFoundException;
import cz.cernicek.eshop.exception.ProductNotFoundException;
import cz.cernicek.eshop.mapper.AuthorMapperImpl;
import cz.cernicek.eshop.mapper.ProductMapper;
import cz.cernicek.eshop.mapper.ProductMapperImpl;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.model.ProductRequestDto;
import cz.cernicek.eshop.repository.AuthorRepository;
import cz.cernicek.eshop.repository.GenreRepository;
import cz.cernicek.eshop.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.List;
import java.util.Optional;

import static cz.cernicek.eshop.domain.mother.ObjectMother.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {ProductMapperImpl.class, AuthorMapperImpl.class, ProductServiceImpl.class})
@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest implements WithAssertions {

    private static final Long id = 1L;



    private static final Product expectedProduct = createProductWithId(id);
    private static final ProductDto expectedResult = createProductDtoWithId(id);
    private static final ProductRequestDto productDtoToSave = createProductRequest();

    private static final Genre genre = expectedProduct.getGenre();
    private static final Author author = expectedProduct.getAuthor();


    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private GenreRepository genreRepository;

    @MockBean
    private AuthorRepository authorRepository;

    @SpyBean
    private ProductMapper productMapper;


    @Autowired
    private ProductServiceImpl productService;

    @Test
    void getAll() {
        // arrange
        final ProductDto productDto1 = createProductDtoWithId(1L);
        final ProductDto productDto2 = createProductDtoWithId(2L);
        final ProductDto productDto3 = createProductDtoWithId(3L);
        final List<ProductDto> expectedResult = List.of(productDto1, productDto2, productDto3);

        final Product product1 = createProductWithId(1L);
        final Product product2 = createProductWithId(2L);
        final Product product3 = createProductWithId(3L);
        final List<Product> listOfProducts = List.of(product1, product2, product3);

        when(productRepository.findAll())
                .thenReturn(listOfProducts);

        // act
        final List<ProductDto> getAll = productService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEqualTo(expectedResult);

        verify(productRepository).findAll();
        verify(productMapper, times(listOfProducts.size())).mapToResponse(any(Product.class));
    }

    @Test
    void getAll_noProducts() {
        // arrange
        final List<Product> listOfProducts = List.of();

        when(productRepository.findAll())
                .thenReturn(listOfProducts);

        // act
        final List<ProductDto> getAll = productService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEmpty();

        verify(productRepository).findAll();
        verifyNoInteractions(productMapper);
    }


    @Test
    void findById() {
        // arrange
        when(productRepository.findById(1L))
                .thenReturn(Optional.of(expectedProduct));

        // act
        final ProductDto byId = productService.findById(1L);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(expectedResult);

        verify(productRepository).findById(1L);
        verify(productMapper).mapToResponse(expectedProduct);
    }

    @Test
    void findById_notFound() {
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> productService.findById(1L));

    }

    @Test
    void saveProduct() {
        // arrange
        final Product mappedProduct = createMappedProduct();

        when(authorRepository.findById(id))
                .thenReturn(Optional.of(author));

        when(genreRepository.findById(id))
                .thenReturn(Optional.of(genre));

        when(productRepository.save(mappedProduct))
                .thenReturn(expectedProduct);


        // act
        final ProductDto saveProduct = productService.saveProduct(productDtoToSave);

        // assert
        assertThat(saveProduct).isNotNull();
        assertThat(saveProduct).isEqualTo(expectedResult);

        verify(authorRepository).findById(id);
        verify(genreRepository).findById(id);
        verify(productRepository).save(mappedProduct);
        verify(productMapper).mapToEntity(productDtoToSave);
        verify(productMapper).mapToResponse(expectedProduct);

    }

    @Test
    void saveProduct_authorNotFound() {
        when(authorRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(AuthorNotFoundException.class, () -> productService.saveProduct(productDtoToSave));
    }

    @Test
    void saveProduct_genreNotFound() {
        when(authorRepository.findById(id))
                .thenReturn(Optional.of(author));

        when(genreRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(GenreNotFoundException.class, () -> productService.saveProduct(productDtoToSave));
    }

    @Test
    void updateProduct() {
        // arrange
        final Product productToSave = createProductWithNullTimestamps(id);

        when(productRepository.existsById(id))
                .thenReturn(true);

        when(authorRepository.findById(id))
                .thenReturn(Optional.of(author));

        when(genreRepository.findById(id))
                .thenReturn(Optional.of(genre));

        when(productRepository.save(productToSave))
                .thenReturn(expectedProduct);

        // act
        final ProductDto result = productService.updateProduct(id, productDtoToSave);

        // assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        verify(productRepository).existsById(id);
        verify(authorRepository).findById(id);
        verify(genreRepository).findById(id);
        verify(productMapper).mapToEntity(productDtoToSave);
        verify(productMapper).mapToResponse(expectedProduct);

    }

    @Test
    void updateProduct_productNotFound() {
        when(productRepository.existsById(id))
                .thenReturn(false);

        assertThrows(ProductNotFoundException.class, () -> productService.updateProduct(id, productDtoToSave));
    }

    @Test
    void updateProduct_authorNotFound() {
        when(productRepository.existsById(id))
                .thenReturn(true);

        when(authorRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(AuthorNotFoundException.class, () -> productService.updateProduct(id, productDtoToSave));
    }

    @Test
    void updateProduct_genreNotFound() {
        when(productRepository.existsById(id))
                .thenReturn(true);

        when(authorRepository.findById(id))
                .thenReturn(Optional.of(author));

        when(genreRepository.findById(id))
                .thenReturn(Optional.empty());

        assertThrows(GenreNotFoundException.class, () -> productService.updateProduct(id, productDtoToSave));
    }

    @Test
    void deleteById() {
        // arrange
        when(productRepository.existsById(id))
                .thenReturn(true);

        // act
        productService.deleteById(id);

        // assert
        verify(productRepository).existsById(id);
        verify(productRepository).deleteById(id);
    }

    @Test
    void deleteById_notFound() {
        when(productRepository.existsById(id))
                .thenReturn(false);

        assertThrows(ProductNotFoundException.class, () -> productService.deleteById(id));
    }
}