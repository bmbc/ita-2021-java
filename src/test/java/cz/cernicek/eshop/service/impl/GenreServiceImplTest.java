package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Genre;
import cz.cernicek.eshop.exception.GenreNotFoundException;
import cz.cernicek.eshop.mapper.GenreMapper;
import cz.cernicek.eshop.model.GenreDto;
import cz.cernicek.eshop.repository.GenreRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static cz.cernicek.eshop.domain.mother.ObjectMother.createGenreDtoWithId;
import static cz.cernicek.eshop.domain.mother.ObjectMother.createGenreWithId;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GenreServiceImplTest implements WithAssertions {

    private static final Long id = 1L;

    @Mock
    private GenreRepository genreRepository;

    @Spy
    private final GenreMapper genreMapper = Mappers.getMapper(GenreMapper.class);

    @InjectMocks
    private GenreServiceImpl genreService;

    @Test
    void getById() {
        // arrange
        final Genre genre = createGenreWithId(id);
        final GenreDto genreDto= createGenreDtoWithId(id);

        when(genreRepository.findById(id))
                .thenReturn(Optional.of(genre));

        // act
        final GenreDto byId = genreService.getById(id);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(genreDto);

        verify(genreRepository).findById(id);
        verify(genreMapper).toDto(genre);
    }

    @Test
    void getById_genreNotFound() {
        when(genreRepository.findById(id))
                .thenReturn(Optional.empty());

        assertThrows(GenreNotFoundException.class, () -> genreService.getById(id));
    }

    @Test
    void getAll() {
        // arrange
        final Genre genre1 = createGenreWithId(1L);
        final Genre genre2 = createGenreWithId(2L);
        final Genre genre3 = createGenreWithId(3L);
        final List<Genre> genreList = List.of(genre1, genre2, genre3);

        final GenreDto genreDto1 = createGenreDtoWithId(1L);
        final GenreDto genreDto2 = createGenreDtoWithId(2L);
        final GenreDto genreDto3 = createGenreDtoWithId(3L);
        final List<GenreDto> genreDtoList = List.of(genreDto1, genreDto2, genreDto3);

        when(genreRepository.findAll())
                .thenReturn(genreList);

        // act
        final List<GenreDto> getAll = genreService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEqualTo(genreDtoList);

        verify(genreRepository).findAll();
        verify(genreMapper, times(genreList.size())).toDto(any(Genre.class));
    }

    @Test
    void getAll_noGenres() {
        // arrange
        final List<Genre> genreList = List.of();

        when(genreRepository.findAll())
                .thenReturn(genreList);

        // act
        final List<GenreDto> getAll = genreService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEmpty();

        verify(genreRepository).findAll();
        verifyNoInteractions(genreMapper);
    }
}