package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.domain.Product;
import cz.cernicek.eshop.exception.CartNotFoundException;
import cz.cernicek.eshop.exception.ProductNotFoundException;
import cz.cernicek.eshop.mapper.CartMapper;
import cz.cernicek.eshop.model.CartDto;
import cz.cernicek.eshop.model.ProductDto;
import cz.cernicek.eshop.repository.CartRepository;
import cz.cernicek.eshop.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static cz.cernicek.eshop.domain.mother.ObjectMother.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest implements WithAssertions {

    private static final Long productId = 1L;
    private static final Long cartId = 1L;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private CartRepository cartRepository;

    @Spy
    private final CartMapper cartMapper = Mappers.getMapper(CartMapper.class);

    @InjectMocks
    private CartServiceImpl cartService;

    @Test
    void createCart() {
        // arrange
        final Product productToAdd = createProductWithId(productId);

        final List<Product> listOfProductsToAdd = List.of(productToAdd);
        final List<ProductDto> listOfDtoProductsToAdd = List.of(createProductDtoWithId(productId));

        final Cart cartToSave = new Cart()
                .setProducts(listOfProductsToAdd);
        final Cart cartWithId = createCartWithProducts(listOfProductsToAdd);
        final CartDto expectedResult = createCartDtoWithProducts(listOfDtoProductsToAdd);

        when(productRepository.findById(productId))
                .thenReturn(Optional.of(productToAdd));

        when(cartRepository.save(cartToSave))
                .thenReturn(cartWithId);

        // act
        final CartDto result = cartService.createCart(productId);

        // assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        verify(productRepository).findById(productId);
        verify(cartRepository).save(cartToSave);
        verify(cartMapper).toDto(cartWithId);
    }

    @Test
    void createCart_productNotFound() {
        when(productRepository.findById(productId))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> cartService.createCart(productId));
    }

    @Test
    void addProduct() {
        // arrange
        final Cart cart = createCartWithThreeProducts();
        final Long cartId = cart.getId();
        final Long productId = 4L;
        final Product productToAdd = createProductWithId(productId);
        final CartDto expectedResult = createCartDtoWithFourProducts();

        when(cartRepository.findById(cartId))
                .thenReturn(Optional.of(cart));

        when(productRepository.findById(productId))
                .thenReturn(Optional.of(productToAdd));

        // act
        final CartDto result = cartService.addProduct(cartId, productId);

        // assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        verify(cartRepository).findById(cartId);
        verify(productRepository).findById(productId);
        verify(cartMapper).toDto(cart);

    }

    @Test
    void addProduct_cartNotFound() {
        when(cartRepository.findById(cartId))
                .thenReturn(Optional.empty());

        assertThrows(CartNotFoundException.class, () -> cartService.addProduct(cartId, productId));
    }

    @Test
    void addProduct_productNotFound() {
        when(cartRepository.findById(cartId))
                .thenReturn(Optional.of(createCartWithThreeProducts()));

        when(productRepository.findById(productId))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> cartService.addProduct(cartId, productId));
    }

    @Test
    void getById() {
        // arrange
        final Cart cart = createCartWithThreeProducts();
        final Long id = cart.getId();
        final CartDto expectedResult = createCartDtoWithThreeProducts();

        when(cartRepository.findById(id))
                .thenReturn(Optional.of(cart));

        // act
        final CartDto result = cartService.getById(id);

        // assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        verify(cartRepository).findById(id);
        verify(cartMapper).toDto(cart);
    }

    @Test
    void getById_cartNotFound() {
        when(cartRepository.findById(cartId))
                .thenReturn(Optional.empty());

        assertThrows(CartNotFoundException.class, () -> cartService.getById(cartId));
    }
}