package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Cart;
import cz.cernicek.eshop.domain.Order;
import cz.cernicek.eshop.exception.CartNotFoundException;
import cz.cernicek.eshop.mapper.OrderMapper;
import cz.cernicek.eshop.model.OrderDto;
import cz.cernicek.eshop.model.OrderRequestDto;
import cz.cernicek.eshop.repository.CartRepository;
import cz.cernicek.eshop.repository.OrderRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static cz.cernicek.eshop.domain.mother.ObjectMother.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest implements WithAssertions {

    @Mock
    private CartRepository cartRepository;

    @Mock
    private OrderRepository orderRepository;

    @Spy
    private final OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    void createOrder() {
        // arrange
        final Cart cart = createCartWithThreeProducts();
        final Long cartId = cart.getId();

        final OrderRequestDto orderRequestDto = createOrderRequestDtoForTesting(cartId);
        final OrderDto expectedResult = createOrderDtoForTesting();
        final Order expectedMappedOrder = createMappedOrderForTesting();
        final Order expectedSavedOrder = createOrderForTesting();

        when(cartRepository.findById(cartId))
                .thenReturn(Optional.of(cart));

        when(orderRepository.save(expectedMappedOrder))
                .thenReturn(expectedSavedOrder);

        // act
        final OrderDto result = orderService.createOrder(orderRequestDto);

        // assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        verify(cartRepository).findById(cartId);
        verify(orderRepository).save(expectedMappedOrder);
        verify(cartRepository).delete(cart);
        verify(orderMapper).toEntity(orderRequestDto);
        verify(orderMapper).toDto(expectedSavedOrder);


    }

    @Test
    void createOrder_noCart() {
        final OrderRequestDto orderRequestDto = createOrderRequestDtoForTesting(1L);

        when(cartRepository.findById(orderRequestDto.getIdCart()))
                .thenReturn(Optional.empty());

        assertThrows(CartNotFoundException.class, () -> orderService.createOrder(orderRequestDto));
    }
}