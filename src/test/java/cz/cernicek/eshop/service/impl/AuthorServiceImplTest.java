package cz.cernicek.eshop.service.impl;

import cz.cernicek.eshop.domain.Author;
import cz.cernicek.eshop.exception.AuthorNotFoundException;
import cz.cernicek.eshop.mapper.AuthorMapper;
import cz.cernicek.eshop.model.AuthorDto;
import cz.cernicek.eshop.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static cz.cernicek.eshop.domain.mother.ObjectMother.createAuthorDtoWithId;
import static cz.cernicek.eshop.domain.mother.ObjectMother.createAuthorWithId;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest implements WithAssertions {

    private static final Long id = 1L;

    @Mock
    private AuthorRepository authorRepository;

    @Spy
    private final AuthorMapper authorMapper = Mappers.getMapper(AuthorMapper.class);

    @InjectMocks
    private AuthorServiceImpl authorService;


    @Test
    void getById() {
        // arrange
        final Author author = createAuthorWithId(id);
        final AuthorDto authorDto= createAuthorDtoWithId(id);

        when(authorRepository.findById(id))
                .thenReturn(Optional.of(author));

        // act
        final AuthorDto byId = authorService.getById(id);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(authorDto);

        verify(authorRepository).findById(id);
        verify(authorMapper).toDto(author);
    }

    @Test
    void getById_authorNotFound() {
        when(authorRepository.findById(id))
                .thenReturn(Optional.empty());

        assertThrows(AuthorNotFoundException.class, () -> authorService.getById(id));
    }

    @Test
    void getAll() {
        // arrange
        final Author author1 = createAuthorWithId(1L);
        final Author author2 = createAuthorWithId(2L);
        final Author author3 = createAuthorWithId(3L);
        final List<Author> authorList = List.of(author1, author2, author3);

        final AuthorDto authorDto1 = createAuthorDtoWithId(1L);
        final AuthorDto authorDto2 = createAuthorDtoWithId(2L);
        final AuthorDto authorDto3 = createAuthorDtoWithId(3L);
        final List<AuthorDto> authorDtoList = List.of(authorDto1, authorDto2, authorDto3);

        when(authorRepository.findAll())
                .thenReturn(authorList);

        // act
        final List<AuthorDto> getAll = authorService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEqualTo(authorDtoList);

        verify(authorRepository).findAll();
        verify(authorMapper, times(authorList.size())).toDto(any(Author.class));
    }

    @Test
    void getAll_noAuthors() {
        // arrange
        final List<Author> authorList = List.of();

        when(authorRepository.findAll())
                .thenReturn(authorList);

        // act
        final List<AuthorDto> getAll = authorService.getAll();

        // assert
        assertThat(getAll).isNotNull();
        assertThat(getAll).isEmpty();

        verify(authorRepository).findAll();
        verifyNoInteractions(authorMapper);
    }
}